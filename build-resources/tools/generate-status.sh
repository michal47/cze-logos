#!/bin/bash

lamedbdir=$1
srindexpath=$2
channelindex=$3
tempfile=/tmp/channelindex
lamedbinput=$4

echo "Downloading lamedb files..."
wget -q -O - http://creimer.net/picons/lamedb_dvbc_germany | grep -A2 '....:.*ffff.*:....:....:.*' | grep -v '\-\-' | iconv -f UTF-8 -t MS-ANSI -c > $lamedbdir/DVB-C_GERMANY
wget -q -O - http://creimer.net/picons/lamedb_192 | grep -A2 '....:.*c0.*:....:....:.*' | grep -v '\-\-' | iconv -f UTF-8 -t MS-ANSI -c > $lamedbdir/19.2E
wget -q -O - http://creimer.net/picons/lamedb_282 | grep -A2 '....:.*11a.*:....:....:.*' | grep -v '\-\-' | iconv -f UTF-8 -t MS-ANSI -c > $lamedbdir/28.2E
cat $lamedbinput/lamedb | grep -A2 '....:.*eb.*:....:....:.*' | grep -v '\-\-' | iconv -f UTF-8 -t MS-ANSI -c > $lamedbdir/23.5E
cat $lamedbinput/lamedb | grep -A2 '....:.*82.*:....:....:.*' | grep -v '\-\-' | iconv -f UTF-8 -t MS-ANSI -c > $lamedbdir/13.0E
#cat $lamedbinput/lamedb | grep -A2 '....:.*e08.*:....:....:.*' | grep -v '\-\-' | iconv -f UTF-8 -t MS-ANSI -c > $lamedbdir/0.8W
#cat $lamedbinput/lamedb | grep -A2 '....:.*c0.*:....:....:.*' | grep -v '\-\-' | iconv -f UTF-8 -t MS-ANSI -c > $lamedbdir/19.2E
#cat $lamedbinput/lamedb | grep -A2 '....:.*11a.*:....:....:.*' | grep -v '\-\-' | iconv -f UTF-8 -t MS-ANSI -c > $lamedbdir/28.2E

echo "Searching for serviceref duplicates..."
duplicates=$(cat $lamedbdir/* | grep -o '^....:........:....:....:.*:.*' | tr [a-z] [A-Z] | sort | uniq -d)
if [ ! -z "$duplicates" ]; then
    echo "The following duplicates are found:"
    echo "$duplicates"
    exit 1
fi

for lamedb in $lamedbdir/* ; do

    linescount=$(grep -h '^....:........:....:....:.*:.*' $lamedb | wc -l)
    currentline=0

    grep -h '^....:........:....:....:.*:.*' $lamedb | while read line ; do
        currentline=$((currentline+1))
        echo -ne "Generating info for ${lamedb##*/} - $currentline/$linescount"\\r
        channelname=$(grep -h -A2 $line $lamedb | sed -n "2p" | sed -e 's/^[ \t]*//' | cut -c1-50)
        providername=$(grep -h -A2 $line $lamedb | sed -n "3p" | sed "s/,.*/ /g" | sed 's/p:/ /g' | sed -e 's/^[ \t]*//' | cut -c1-25)

        if [ -z "$channelname" ]; then
            channelname="----------"
        fi

        if [ -z "$providername" ]; then
            providername="----------"
        fi
        
        serviceref=(${line//:/ })
        
        if [ -z "$(echo ${serviceref[3]} | sed 's/0*//')" ]; then
            service_3="0"
        else
            service_3="$(echo ${serviceref[3]} | sed 's/0*//')"
        fi

        serviceref=$(echo ${serviceref[0]} | sed 's/0*//')"_"$(echo ${serviceref[2]} | sed 's/0*//')"_"$service_3"_"$(echo ${serviceref[1]} | sed 's/0*//')
        serviceref=$(echo $serviceref | tr [a-z] [A-Z])
        servicetype=$(printf "%x\n" ${serviceref[4]} | tr [a-z] [A-Z])
        
        case $servicetype in
            1|11|16|19)
            servicetype="TV";;
            2|A)
            servicetype="RADIO";;
            *)
            servicetype="TYPE-$servicetype";;
        esac

        picon="----------"
        
        for file in $srindexpath/*.srindex ; do
            i=$(grep -h -n ^$serviceref $file)

            if ! [ -z "$i" ]; then
                i=(${i//:/ })
                i=${i[0]}

                until sed -n $i"p" $file | grep -q '#'; do 
                    i=$((i-1))
                done

                picon=$(sed -n $i"p" $file)
                file=${file##*/}
                picon=${file%.*}/${picon#??}
            fi
        done

        sat=$(echo ${lamedb##*/} | sed 's/_/ /g')
        echo -e "$channelname\t$providername\t$servicetype\t$serviceref\t$picon\t$sat" >> $tempfile"_"${lamedb##*/}

    done

    sat=$(echo ${lamedb##*/} | sed 's/_/ /g')
    lamedbupdate=$lamedbupdate"$sat\n"
    echo ""

done

for lamedb in $lamedbdir/* ; do
    sat=$(echo ${lamedb##*/} | sed 's/_/ /g')
    echo -e "##################################################" >> $tempfile"_temp"
    echo -e "########### $sat" >> $tempfile"_temp"
    echo -e "##################################################" >> $tempfile"_temp"
    cat $tempfile"_"${lamedb##*/} | sort -t $'\t' -k 3,3 -k 1,1 >> $tempfile"_temp"
done

cat $tempfile"_temp" | iconv -f MS-ANSI -t UTF-8 -c | column -t -s $'\t' > $tempfile"_final"
echo -e "Generated on:\n$(date +"%Y-%m-%d %H:%M:%S")\n\nIncluded:\n$lamedbupdate\n\n$(cat $tempfile\_final)" > $channelindex
rm $tempfile*
